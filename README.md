## Introduction
This is a simple template to pipeline zola static site generator with GitLab Pages.
[Getzola.org](https://www.getzola.org)

It is basically the same as [the official deployment procedure from the zola wiki](https://www.getzola.org/documentation/deployment/gitlab-pages/).
